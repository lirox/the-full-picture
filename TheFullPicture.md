# ﷽

In the name of Allāh, the Entirely Merciful, the Especially Merciful.

Translation of Kamilul-Surah, The Full Picture, By Shaykh Ahmadul Sayid.
Translation by @ebnkathir, And contribution from opensource commits.

**Opening:**

All the praises and thanks be to Allâh, the Lord of the ‘Âlamîn (mankind, jinn and all that exists), And confer, O Allah, blessings upon the master of those who had been sent, Muhammad. And upon his noble family and his compliant companions, **proceeding:**

The story of this book began on the Tenth of Rabīʿ ath-Thānī year 1433 (1433 H / 2012 AD), When I uploaded through my Youtube channel the first episode of the video series titled: "The Full Picture"

And it hadn’t crossed my mind then that this series would extend until the date of writing these lines, That corresponds to the middle of Ṣafar 1439 (1439 H / 2013 AD)

And during these six years, Many paths opened up for me through "The Full Picture" programme, Perhaps the most important of which was the continous communication with a wide group of young people that are originally the target group of the programme, Which has had a big effect on expanding my knowledge of what occupies the minds of the youth ranging from questions and mental challenges that are renewed and accelerated through the huge pumps of social media.

And the chains of communcation expanded from both sides until the big coronation represented by the programme "The Preparing of the Discourser" (ṣenaʾat al-muḥāwir) which at the type of writing is being worked on by a group of students of knowledge and virtious shuyukh on one side, And is benefiting thousands on the other side. And all thanks is to Allah, Elevated is He.

**The dilemma of choosing a title for the book:**

It is not a secret to anyone who has authored a book or produced a programme before that choosing a title for the book or the programme poses a big challenge, For combining:

- Seriousness,
- Suspense, 
- And indicating the content inside of the book,
- With condensation, (And these are the requirements of a good title) greatly decreases the options we can choose from and uses up mental energy, And often do programme producers hold consulting meetings to choose a title, Which are known as "Brainstorming" meetings.

In any case, I have done this brainstorming myself before publishing the first episode of the programme, Because of my lack of knowledge – at that point – of who shares the same intrest with me in the field of that programme. Then from the blessings of starting in this field [of guiding the youth] I met people that were truly intrested in it. And solid relations were established between us with thanks to Allah, Elevated is He, So I was blessed with collegues that are close to my heart, Indeed they are from the hurried bliss that exists in this wordly life.

I set out in choosing a title based on the origin of the issues that I noticed in those who are affected by the spreading doubts against Islamic principles, And it is nothing but not encompassing knowledge about the issue which feels doubtful, Really looking at it from a shallow point of view reveals a part of the picture and hides the rest of it.

So I chose – After long periods of thinking – The title **"The Full Picture"** signaling to the importance of having complete perception towards issues that are covered by discussion and debates.

**From Video to Book:**

After completing sixteen recorded episodes from the programme, I got the idea of releasing it in a written, organized format in the wording that is suitable for production into a book, And this was done – Thanks to Allah, Elevated is He, - And the book was published in the international book fair in Al-Riyadh year 1435, (1435 H / 2014 AD) Then succeding it was the second part, In the year after it. Encompassing eight new episodes.

And I praise and thank Allah, Elevated is He, over what I saw of care for the two parts of the book by the youth in book clubs and other places, As it also made me happy to see some virtous university professors issuing some episodes from the programme upon the students, Or showing it or guiding to it.

**And after the book sold out from the market the brothers at Takween centre wanted to re-print the book and suggested to me that we combine the two parts in one book, And I liked the idea, And I began work on re-organizing the book and releasing it, And I had thought that I would be done with that in a short period of time, And I told that to the organizers of educational curriculums that repeatedly asked about the book, But I was suprised to see that the release takes up a lot of time, Especially since I added to the book many new things, And new different varieties of topics, And what prevented me from completing it in a short period of time was the large number of diversions.**

And here is the book after combining its two parts and after re-organizing and release. And after adding to it many new additions over the original, And after removing many topics from it, I present it to the dear readers in this first copy, Which may be succeded with other copies.

And I ask Allah, The most Generous Gracious And Bountiful, for support and guidance and blessing and sucess, And confer, O Allah, Blessings upon our prophet Muhammad.

Ahmad bin Yousef Al-Sayeed
<Alsaiyd98@gmail.com>

**The first Chapter; Proofs of the Principles of Islam:**

**-**

**Firstly : Proving the existence of Allah.**
**Secondly: The purpose of [our] existence.**
**Thirdly: Proving the Prophethood and the validity of the Qur’an.**
**Fourthly: The beauties of Islam..**

**-**

**Firstly: Proving the Existence of Allah.**

Whenever the agnostic one asks: "What is the proof for the existence of Allah" before even answering his question, it's crucial to clarify his criteria for satisfactory proof of Allah’s existence. Glorified and Elevated is He; For many agnostics – Thanks to denying atheists – leave the easy and clear answers regarding the existence of Allah, And demand long, rugged routes that don’t even come to a conclusion. And they may have conditions – For the proof that leads to knowing Allah’s [existence] – That ultimately come down to their personal opinions and taste, And their personal approval. And not-
set according to methodological standards, Or objective scales.

So these people in particular, However much proofs or evidences you mention indeed it will not benefit them as long as it doesn’t fit their personal approvals that they have themselves put in place, And that is like what the children of Israel have said to Moses, Peace be upon him, *"O Moses, we will never believe you until we see Allāh outright"* (Q2:55) so here they have narrowed the potential evidence [for Allah’s existence] to merely faith in sensory vision, And this is intransigence that the stubborn from among the disbelievers in Allah and his Messanger have inherited from them throughout all time, Up to our time.

While if you reflect upon the evidence and proof that lead to the general objective truths in various fields; You would find that it doesn’t narrow down absloutely to only sensory vision, For example: Our absloute faith in the existence of historical figures like Plato and Aristotle and Saladin is not only proved through sensory vision, Or direct sensory expeirence, For its main pillar is narration – Which one of the sources of knowledge. – 

Certainly proving the existence of Allah, Glorified and Elevated is He, Is something innate to the human instinct, The mind does not strain itself or stress to get to it. For it is established on a principle that the human finds centered in his mind so much that he absloutely cannot get rid of it. And it is "Infering cause from effect" but rather most atheists that deny the existence of the Creator, Glorified is He, apply this principle in all other aspects of their lives, Even if they deny it in Theology. 

Searching beyond effects, actions and events for effectors, agents and causes cannot be abandoned by humans - at all – Except if they lose their minds, How so when humans cannot imagine the occurance of things after their non-existence without the existence of reasons for that occurance that suit the nature of that occurance?

And if the human mind does not accept imagining the occurance of a picture of a perfect human on a white piece of paper without the existence of a reason that has the ability that allows it to create the picture on the paper, Then him [the human] not accepting the occurance of the human – I.e himself – without the existence of an agent that is capable and knowing = is first and foremost.

**And say the same about all other caused things after their inexistence.**

so here they have narrowed the potential evidence [for Allah’s existence] to merely faith in sensory vision, And this is intransigence that the stubborn from among the disbelievers in Allah and his Messanger have inherited from them throughout all time, Up to our time.

While if you reflect upon the evidence and proof that lead to the general objective truths in various fields; You would find that it doesn’t narrow down absloutely to only sensory vision, For example: Our absloute faith in the existence of historical figures like Plato and Aristotle and Saladin is not only proved through sensory vision, Or direct sensory expeirence, For its main pillar is narration – Which one of the sources of knowledge. – 

Certainly proving the existence of Allah, Glorified and Elevated is He, Is something innate to the human instinct, The mind does not strain itself or stress to get to it. For it is established on a principle that the human finds centered in his mind so much that he absloutely cannot get rid of it. And it is "Infering cause from effect" but rather most atheists that deny the existence of the Creator, Glorified is He, apply this principle in all other aspects of their lives, Even if they deny it in Theology. 

Searching beyond effects, actions and events for effectors, agents and causes cannot be abandoned by humans - at all – Except if they lose their minds, How so when humans cannot imagine the occurance of things after their non-existence without the existence of reasons for that occurance that suit the nature of that occurance?

And if the human mind does not accept imagining the occurance of a picture of a perfect human on a white piece of paper without the existence of a reason that has the ability that allows it to create the picture on the paper, Then him [the human] not accepting the occurance of the human – I.e himself – without the existence of an agent that is capable and knowing = is first and foremost.

**And say the same about all other caused things after their inexistence.**

And what is meant by "Deviation" in the subject of sources of knowledge: It is narrowing the valid manners of obtaining knowledge into one source and neglecting all other sources.

And the most important sources that knowledge can be built upon is: The mind, And the senses – Like seeing and touching -  And the correct authentic narration, And we also gain from our human instinct primary knowledge that are considered foundations for many secondary knowledge that we gain later on.

**An example for deviation concerning sources of knowledge:**

 If you say to the Atheist: We believe in the existence of Allah, Glorified is He, Based on definitive evidence of reason, And [based on] human instinct.

And he says to you: But I do not trust in anything other than sensory experience that are built upon observation in a controlled enviroment as proof. And since this does not apply to the evidence for the existence of Allah, Therefore he doesn’t believe in Him.

The reason for this Atheism is because of the deviation in the field of the sources of knowledge, Untop of arrogance – That very little are spared from – that prevents him from submitting to the truth that requires denying your desires.

**The second matter: Not grasping the reality of the evidence of Believers and reconciling it with the doubts of Atheists**

Frequently does the Atheist make fun of the evidences of believers on the existence of Allah, So they imagine it in a way that contradicts its reality, Until the ignorant one believes that the evidences used by Believers is just logical fallacies and fanaticism and stubborn faith, And this is not true.

And likewise is: Reconciling [or equating] between the rational instinctive principle **(That every occurance must have a cause)**

and between a ridiculos imaginary hypothesis that Bertrand Russell decided on, Imagining with it that the probablity of the existence of Allah is like the probability of the existence of a flying kettle in outer space, As in it cannot be proven or negated, And this is a fallacy that stems from equating between two completely different things, For the kettle has no cause, But everything we see in the universe is caused by the existence of Allah and his Self-Subsistance.

**The third matter: Claiming incorrect results, because they lack the condition of concomitance**

From the conditions of the validity of the deduction is that the result must be deduced from the evidence by necessity, While deducing unconcomitant results using correct evidence is an error that many Atheists and those who spread doubts against the Sunnah (Prophetic Tradition) and [other] principles.  
