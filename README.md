# ﷽

The Full Picture, As-salamu ʿalaykum wa-rahmatullahi wa-barakatuh.

This is a translation from Arabic to English, We are translating Shaykh Ahmad Al-Sayid's "Kamilul Surah"

This is opensourced so that you can suggest edits to the book. :)

You can get started by making an account on gitlab, And clicking on the "TheFullPicture.md" above and editing it and then
commiting your edit, And I will review your edit and accept it or reject it or discuss it with you :)

دمتم في أمان الله | May you always stay in the protection of Allah.
